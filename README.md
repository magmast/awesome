# Magmast Awesome

My own awesome list.

## Devops

- [waypoint](https://github.com/hashicorp/waypoint) - Waypoint provides a modern
  workflow to build, deploy, and release across platforms.
- [sftpgo](https://github.com/drakkan/sftpgo) - Fully featured and highly configurable
  SFTP server with optional FTP/S and WebDAV support - S3, Google Cloud Storage, Azure
  Blob.

## Cross platorm development

- [tauri](https://github.com/tauri-apps/tauri) - Build smaller, faster, and more
  secure desktop applications with a web frontend.

## Styling

- [linaria](https://github.com/callstack/linaria) - Zero-runtime CSS in JS library
